﻿#include "Hero.h"

#include <iostream>

#include "Enemy.h"

Hero::Hero(const std::string & name, const std::string & class_type, int health, int money) :
    Character(name, health),
    _inventory(),
    _classType(class_type),
    _money(money),
    _xp(0)
{

}

void Hero::Attack(Enemy & enemy)
{
    Character::Attack(enemy);

    std::cout << GetName() << " hit the " << enemy.GetName() << " [" << hitValue << "]\n";

    if (enemy.GetHealth() <= 0)
    {
        std::cout << "The " << enemy.GetName() << " is dead\n";
        std::cout << "    " << GetName() << " gains " << enemy.GetReward().GetCoins() << " coins\n";
        _money += enemy.GetReward().GetCoins();
        std::cout << "    "  << GetName() << " gains " << enemy.GetReward().GetXp() << " xp\n";
        _xp += enemy.GetReward().GetXp();
    }
}

void Hero::Display() const
{
    std::cout << "------------------------------\n";
    std::cout << "|| " << GetName() << ", the " << _classType << "\n";
    std::cout << "------------------------------\n";
    std::cout << "    Health:" << GetHealth() << "\n";
    std::cout << "    Level:" << GetLevel() << "\n";
    std::cout << "    XP:" << _xp << "\n";
    std::cout << "    Money:" << _money << "\n";
    _inventory.Display();
    std::cout << "------------------------------\n";
}

int Hero::GetAttackValue() const
{
    return _inventory.GetAttackValue();
}

int Hero::GetDefenseValue() const
{
    return _inventory.GetDefenseValue();
}

Inventory & Hero::GetInventory()
{
    return _inventory;
}

const std::string& Hero::GetClassType() const
{
    return _classType;
}