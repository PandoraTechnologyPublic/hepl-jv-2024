﻿#include "Item.h"

#include <iostream>

Item::Item() :
    _itemName("Uninitialised"),
    _armorValue(-1)
{

}

Item::Item(const std::string & itemName, int armorValue) :
    _itemName(itemName),
    _armorValue(armorValue)
{
    _itemName = itemName;
    _armorValue = armorValue;
}

void Item::Display() const
{
    std::cout << _itemName << "(AC:" << _armorValue << ")";
}
