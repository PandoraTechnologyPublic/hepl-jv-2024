﻿#pragma once
#include "Character/Character.h"
#include "Locations/City.h"

class World
{
private:
    City _city;
public:
    World();

    void Explore(Character & character);
};
