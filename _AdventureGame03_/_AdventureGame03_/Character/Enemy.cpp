﻿#include "Enemy.h"

#include <iostream>

#include "Character.h"
#include "Hero.h"
#include "../Reward.h"

Enemy::Enemy(const std::string & name, int health, int damage_value, int defense_value) :
    Character(name, health),
    _attackValue(damage_value),
    _defenseValue(defense_value),
    _reward(-1, -1)
{
}

void Enemy::AddReward(const Reward & reward)
{
    _reward = reward;
}

int Enemy::GetAttackValue() const
{
    return _attackValue;
}

int Enemy::GetDefenseValue() const
{
    return _defenseValue;
}

const Reward& Enemy::GetReward() const
{
    return _reward;
}

void Enemy::Attack(Hero & hero) const
{
    Character::Attack(hero);

    std::cout << "The " << GetName() << " hit " << hero.GetName() << " [" << hitValue << "]\n";

    if (hero.GetHealth() <= 0)
    {
        std::cout << hero.GetName() << ",the " << hero.GetClassType() << " is dead\n";
    }
}
