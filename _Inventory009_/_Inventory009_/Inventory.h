﻿#pragma once
#include "Item.h"

const int MAX_ITEMS = 3;

class Inventory
{
private:
    Item _items[MAX_ITEMS];
    int _numberOfItems = 0;

public:
    void AddItem(const Item & item);
    void Display() const;
};