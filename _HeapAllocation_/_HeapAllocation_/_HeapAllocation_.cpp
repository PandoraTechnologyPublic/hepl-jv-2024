
#include <iostream>

#include "Inventory.h"

int main(int argc, char* argv[])
{
    Inventory inventory;

    Item * item = inventory.GetStackAllocatedItem();

    (* item).DisplayId();

    std::cout << item << "\n";

    delete item;

    std::cout << item << "\n";

    // (* item).DisplayId();

    return 0;
}
