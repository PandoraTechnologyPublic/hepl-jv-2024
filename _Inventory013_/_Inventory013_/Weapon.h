﻿#pragma once
#include <string>

#include "Item.h"

class Weapon : public Item
{
private:
    int _damageValue;
public:
    Weapon();
    Weapon(const std::string & name, int damage_value, int price);

    int GetDamageValue() const;

    void Display() const override;
};
