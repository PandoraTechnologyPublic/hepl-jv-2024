﻿#pragma once
#include <string>

class GameOption
{
private:
    std::string _optionText;

    GameOption();
public:
    GameOption(const std::string & option_prompt_text);
};
