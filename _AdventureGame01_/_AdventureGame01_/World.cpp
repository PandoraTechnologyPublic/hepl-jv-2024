﻿#include "World.h"

#include <iostream>

#include "Locations/Dungeon.h"

World::World() :
    _id(std::rand()),
    _city("Rieju")
{

}

void World::Explore()
{
    while (true)
    {
        std::string player_choice;

        std::cout << "======================================" << '\n';
        std::cout << "= You enter the world ID: " << _id << "" << '\n';
        std::cout << "======================================" << '\n';
        std::cout << "1. Enter the city" << '\n';
        std::cout << "2. Enter a dungeon" << '\n';
        std::cout << "3. Exit the world" << '\n';
        std::cout << "===================" << '\n';
        std::cout << "Enter your choice: " << '\n';
        std::cout << "===================" << '\n';
        std::cin >> player_choice;

        if (player_choice == "1")
        {
            _city.Enter();
        }
        else if (player_choice == "2")
        {
            Dungeon dungeon;

            dungeon.Enter();

            break;
        }
        else if (player_choice == "3")
        {
            std::cout << "==================" << '\n';
            std::cout << "You left the world" << '\n';
            std::cout << "==================" << '\n';

            break;
        }
        else
        {
            std::cout << "Invalid choice. Please try again." << '\n';
        }
    }
}
