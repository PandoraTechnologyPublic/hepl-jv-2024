﻿#pragma once

#include "Location.h"

#include "../Items/Item.h"

class Shop : public Location
{
private:
    static const int MAX_ITEM_COUNT = 16;
    Item _items[MAX_ITEM_COUNT];
    int _itemCount;
public:
    Shop();

    void Add(const Item & item);

    void Enter(Character & character) override;

    void ShowGoods(Character & character);
};
