﻿#pragma once
#include "Locations/City.h"

class World
{
private:
    int _id;
    City _city;
public:
    World();

    void Explore();
};
