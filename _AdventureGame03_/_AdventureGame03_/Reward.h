﻿#pragma once

class Reward
{
private:
    int _coins;
    int _xp;

public:
    Reward(int coins, int xp);
    int GetXp() const;
    int GetCoins() const;
};
