﻿#pragma once
#include <vector>

#include "Location.h"

#include "../Items/Item.h"

class Shop : public Location
{
private:
    std::vector<Item> _items;
public:
    Shop();

    void Enter(Character & character) override;

    void ShowGoods(Character & character);
};
