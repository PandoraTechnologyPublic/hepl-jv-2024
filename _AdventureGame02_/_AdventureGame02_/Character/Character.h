﻿#pragma once
#include <string>

class Character
{
private:
    std::string _name;
    int _money;
public:
    Character(const std::string & name, int money);
    int GetMoney() const;
    void Pay(int price);
    std::string GetName() const;
};
