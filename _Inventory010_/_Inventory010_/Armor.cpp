﻿#include "Armor.h"

#include <iostream>

Armor::Armor() :
    _name("Uninitialised"),
    _description("Uninitialised"),
    _price(-1),
    _defenseValue(-1)
{

}

Armor::Armor(const std::string & armor_name, int defense_value, int armor_price) :
    _name(armor_name),
    _description("No description!"),
    _price(armor_price),
    _defenseValue(defense_value)
{
}

void Armor::SetDescription(const std::string& armor_description)
{
    _description = armor_description;
}

void Armor::Display() const
{
    std::cout << "    " << "    " << " - " << _name << " [Defense:" << _defenseValue << "] [Price: " << _price << "]\n";
    std::cout << "    " << "    " << "    " << _description << "\n";
}
