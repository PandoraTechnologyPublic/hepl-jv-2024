﻿#pragma once
#include "GameOption.h"

class GamePrompt
{
private:
    GameOption _gameOptions[4];
public:
    void AddGameOption(GameOption & game_option);
    void DisplayPlayerChoices();
};
