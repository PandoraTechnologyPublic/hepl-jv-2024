﻿#pragma once
#include <string>

class Item
{
private:
    std::string _name;
    std::string _description;
    int _price;
public:
    Item();
    Item(const std::string & name, int price);

    const std::string & GetName() const;

    void SetDescription(const std::string & item_description);

    void Display() const;
};
