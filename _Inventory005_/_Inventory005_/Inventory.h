﻿#pragma once
#include "Item.h"

const int MAX_ITEMS = 3;

class Inventory
{
public:
    Item _items[MAX_ITEMS];
    int _numberOfItems = 0;
};

void AddItem(Inventory & inventory, const Item & item);
void DisplayInventory(const Inventory & inventory);