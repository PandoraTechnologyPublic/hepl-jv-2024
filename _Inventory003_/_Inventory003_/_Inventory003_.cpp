#include <iostream>
#include <string>

const int MAX_ITEMS = 3;

class Item
{
public:
    std::string _itemName;
    int _armorValue;
    int _itemQuantity;
};

class Inventory
{
public:
    Item _items[MAX_ITEMS];
    int _numberOfItems = 0;
};

void AddItem(Inventory & inventory, const Item & item, int item_count)
{
    inventory._items[inventory._numberOfItems]._itemName = item._itemName;
    inventory._items[inventory._numberOfItems]._armorValue = item._armorValue;
    inventory._items[inventory._numberOfItems]._itemQuantity = item_count;

    ++inventory._numberOfItems;
}

void DisplayInventory(const Inventory & inventory)
{
    std::cout << "================" << "\n";
    for (int item_index = 0; item_index < inventory._numberOfItems; ++item_index)
    {
        std::cout << "Item " << item_index << ": " << inventory._items[item_index]._itemName << "(AC:" << inventory._items[item_index]._armorValue << ") [quantity " << inventory._items[item_index]._itemQuantity << "]\n";
    }
}

int main(int argc, char* argv[])
{
    Inventory warrior_inventory;
    Item helmet;
    helmet._itemName = "Helmet";
    helmet._armorValue = 3;
    Item armor;
    armor._itemName = "Armor";
    armor._armorValue = 5;
    Item boots;
    boots._itemName = "Boots";
    boots._armorValue = 2;

    AddItem(warrior_inventory, helmet, 1);
    AddItem(warrior_inventory, armor, 1);
    AddItem(warrior_inventory, boots, 1);

    DisplayInventory(warrior_inventory);

    Inventory mage_inventory;
    Item gloves;
    gloves._itemName = "Gloves";
    gloves._armorValue = 1;
    Item ring;
    ring._itemName = "Ring";
    ring._armorValue = 0;

    AddItem(mage_inventory, gloves, 1);
    AddItem(mage_inventory, ring, 1);
    AddItem(mage_inventory, armor, 1);

    DisplayInventory(warrior_inventory);
    DisplayInventory(mage_inventory);

    return 0;
}
