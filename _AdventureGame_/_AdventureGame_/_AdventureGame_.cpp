#include <iostream>

int main(int argc, char* argv[])
{
    while (true)
    {
        std::string player_choice;

        std::cout << "1. Explore the world\n";
        std::cout << "2. Quit the game\n";
        std::cout << "Enter your choice:\n";

        std::cin >> player_choice;

        if (player_choice == "1")
        {
            std::cout << "This world looks pretty but it is quite limited now!\n";
        }
        else if (player_choice == "2")
        {
            std::cout << "Goodbye\n";
            break;
        }
        else
        {
            std::cout << "Wrong value, please try AGAIN!\n";
        }
    }
    
    return 0;
}
