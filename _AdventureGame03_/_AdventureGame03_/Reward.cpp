﻿#include "Reward.h"

Reward::Reward(int coins, int xp)
{
    _coins = coins;
    _xp = xp;
}

int Reward::GetXp() const
{
    return _xp;
}

int Reward::GetCoins() const
{
    return _coins;
}