﻿#pragma once
#include <string>

#include "Character.h"
#include "Hero.h"
#include "../Reward.h"

class Character;

class Enemy : public Character
{
private:
    int _attackValue;
    int _defenseValue;
    Reward _reward;

public:
    Enemy(const std::string & name, int health, int damage_value, int defense_value);
    void AddReward(const Reward & reward);

    void Attack(Hero & hero) const;
    int GetAttackValue() const override;
    int GetDefenseValue() const override;
    const Reward & GetReward() const;
};
