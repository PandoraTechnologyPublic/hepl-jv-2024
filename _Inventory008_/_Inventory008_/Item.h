﻿#pragma once
#include <string>

class Item
{
public:
    std::string _itemName;
    std::string _itemDescription;
    int _armorValue;

    Item();
    Item(const std::string & itemName, int armorValue);
};
