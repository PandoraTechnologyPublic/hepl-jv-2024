﻿#include "Item.h"

Item::Item() :
    _itemName("Uninitialised"),
    _armorValue(-1)
{

}

Item::Item(const std::string & itemName, int armorValue) :
    _itemName(itemName),
    _armorValue(armorValue)
{
    _itemName = itemName;
    _armorValue = armorValue;
}