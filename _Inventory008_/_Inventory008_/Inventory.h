﻿#pragma once
#include "Item.h"

const int MAX_ITEMS = 3;

class Inventory
{
public:
    Item _items[MAX_ITEMS];
    int _numberOfItems = 0;

    void AddItem(const Item & item);
    void Display() const;
};