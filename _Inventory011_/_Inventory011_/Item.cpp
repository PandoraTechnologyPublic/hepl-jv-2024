﻿#include "Item.h"

#include <iostream>

Item::Item() :
    _name("Uninitialised"),
    _description("Uninitialised"),
    _price(-1)
{
}

Item::Item(const std::string& name, int price) :
    _name(name),
    _description("No description!"),
    _price(price)
{
}

const std::string& Item::GetName() const
{
    return _name;
}

void Item::SetDescription(const std::string& item_description)
{
    _description = item_description;
}

void Item::Display() const
{
    std::cout << "    " << "    " << " - " << _name << " [Price: " << _price << "]\n";
    std::cout << "    " << "    " << "    " << _description << "\n";
}
