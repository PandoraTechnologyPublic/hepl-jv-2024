﻿#pragma once
#include <string>

#include "Item.h"

class Armor : public Item
{
private:
    int _defenseValue;
public:
    Armor();
    Armor(const std::string & name, int defense_value, int price);

    int GetDefenseValue() const;

    void Display() const;
};
