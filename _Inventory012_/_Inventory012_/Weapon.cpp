﻿#include "Weapon.h"

#include <iostream>


Weapon::Weapon() :
    Item(),
    _damageValue(-1)
{
}

Weapon::Weapon(const std::string & name, int damage_value, int price) :
    Item(name, price),
    _damageValue(damage_value)
{
}

int Weapon::GetDamageValue() const
{
    return _damageValue;
}

void Weapon::Display() const
{
    std::cout << "===============\n";
    std::cout << "= WEAPON INFO =\n";
    std::cout << "===============\n";
    std::cout << "    " << "    " << "Weapon info: " << "[Damage:" << _damageValue << "]\n";
    Item::Display();

}

