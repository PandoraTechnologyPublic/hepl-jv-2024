﻿#pragma once
#include "Character.h"
#include "../Inventory.h"

class Hero : public Character
{
private:
    std::string _classType;
    Inventory _inventory;
    int _xp;
    int _money;
public:
    Hero(const std::string & name, const std::string & class_type, int health, int money);

    const std::string & GetClassType() const;
    Inventory & GetInventory();
    int GetAttackValue() const override;
    int GetDefenseValue() const override;

    void Display() const;

    void Attack(Enemy & enemy);
    
};
