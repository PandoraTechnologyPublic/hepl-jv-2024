﻿#include "Shop.h"

#include <iostream>

#include "../Character/Character.h"
#include "../Items/Item.h"

Shop::Shop() :
    Location("Shop"),
    _itemCount(0),
    _items()
{
    Add(Item("Horse", 1000));
    Add(Item("Spaceship", 100000));
}

void Shop::Add(const Item & item)
{
    if (_itemCount < MAX_ITEM_COUNT)
    {
        _items[_itemCount] = item;
        ++_itemCount;
    }
    else
    {
        std::cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << "\n";
        std::cout << "!! ERROR: Shop is full! !!" << "\n";
        std::cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << "\n";
    }
}

void Shop::Enter(Character & character)
{
    Location::Enter(character);

    while (true)
    {
        std::string player_choice;

        std::cout << "======================================" << '\n';
        std::cout << "1. Browse the shop goods" << '\n';
        std::cout << "2. Sell your goods" << '\n';
        std::cout << "3. Quit" << '\n';
        std::cout << "===================" << '\n';
        std::cout << "Enter your choice: " << '\n';
        std::cout << "===================" << '\n';
        std::cin >> player_choice;

        if (player_choice == "1")
        {
            ShowGoods(character);
        }
        else if (player_choice == "2")
        {
            std::cout << "===========================================" << '\n';
            std::cout << "You do not have anything to sell for now!" << '\n';
            std::cout << "===========================================" << '\n';
        }
        else if (player_choice == "3")
        {
            std::cout << "=================" << '\n';
            std::cout << "You left the shop" << '\n';
            std::cout << "=================" << '\n';

            break;
        }
        else
        {
            std::cout << "INVALID CHOICE! Please try again." << '\n';
            std::cout << "===========================================" << '\n';
        }
    }
}

void Shop::ShowGoods(Character & character)
{
    while (true)
    {
        {
            int item_index;

            for (item_index = 0; item_index < _itemCount; ++item_index)
            {
                std::cout << item_index << ". ";
                _items[item_index].Display();
            }

            std::cout << item_index << ". " << "Not buying anything!\n";
            std::cout << "===================" << '\n';
            std::cout << "Enter your choice: " << '\n';
            std::cout << "===================" << '\n';
        }

        std::string player_choice_text;

        std::cin >> player_choice_text;

        int player_choice = -1;

        try
        {
            player_choice = std::stoi(player_choice_text);
        }
        catch (...)
        {
            player_choice = -1;
        }

        if ((player_choice >= 0) && (player_choice <= _itemCount))
        {
            if (player_choice < _itemCount)
            {
                int item_price = _items[player_choice].GetPrice();

                std::cout << "===========================================" << '\n';
                std::cout << "You choose item " << player_choice << " : " << _items[player_choice].GetName() << " @ " << item_price << '\n';
                std::cout << "===========================================" << '\n';

                if (item_price < character.GetMoney())
                {
                    character.Pay(item_price);
                    std::cout << character.GetName() << " pays " << item_price << '\n';
                    std::cout << character.GetName() << " still have " << character.GetMoney() << " MONEY\n";

                    if (player_choice < _itemCount - 1)
                    {
                        _items[player_choice] = _items[_itemCount - 1];
                    }

                    --_itemCount;
                }
                else
                {
                    std::cout << "But you do not have enough money!" << '\n';
                    std::cout << "===========================================" << '\n';
                }

                break;
            }
            else if (player_choice == _itemCount)
            {
                break;
            }
        }
        else
        {
            std::cout << "INVALID CHOICE! Please try again." << '\n';
            std::cout << "===========================================" << '\n';
        }
    }

}
