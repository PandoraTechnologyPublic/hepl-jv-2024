
#include "Item.h"
#include "Rogue.h"
#include "Warrior.h"

int main(int argc, char* argv[])
{
    Warrior warrior;

    Item helmet("Helmet", 3);
    Item armor("Armor", 5);
    Item boots("Boots", 2);

    warrior.inventory.AddItem(helmet);
    warrior.inventory.AddItem(armor);
    warrior.inventory.AddItem(boots);

    warrior.inventory.Display();


    Rogue rogue;

    Item gloves("Gloves", 1);
    Item ring("Ring of Cheating", 0);
    Item dagger("Dagger", 0);

    rogue.inventory.AddItem(gloves);
    rogue.inventory.AddItem(ring);
    rogue.inventory.AddItem(dagger);

    rogue.inventory.Display();

    rogue.Cheat();

    rogue.inventory.Display();
}
