﻿#pragma once
#include "Location.h"

class City : public Location
{
private:
public:
    City(const std::string & name);

    void Enter() override;
};
