﻿#pragma once
#include <string>

class Item
{
private:
    std::string _name;
    int _price;

    Item();
public:
    Item(const std::string & name, int price);
    void Display() const;
    std::string GetName() const;
    int GetPrice() const;
};
