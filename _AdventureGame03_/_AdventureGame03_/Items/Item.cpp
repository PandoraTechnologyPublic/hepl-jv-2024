﻿#include "Item.h"

#include <iostream>

Item::Item() :
    _name("NOT INITIALISED"),
    _price(-1)
{
}

Item::Item(const std::string & name, int price) :
    _name(name),
    _price(price)
{
}

void Item::Display() const
{
    std::cout << _name << " : " << _price << " MONEY\n";
}

std::string Item::GetName() const
{
    return _name;
}

int Item::GetPrice() const
{
    return _price;
}
