﻿#pragma once
#include <string>

class Weapon
{
private:
    std::string _name;
    std::string _description;
    int _price;
    int _damageValue;

public:
    Weapon();
    Weapon(const std::string & weapon_name, int damage_value, int weapon_price);

    void SetDescription(const std::string & weapon_description);

    void Display() const;
};
