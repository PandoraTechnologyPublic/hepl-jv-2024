﻿#include "Item.h"

#include <iostream>

Item::Item() :
    _name("Uninitialised"),
    _description("Uninitialised"),
    _price(-1)
{

}

Item::Item(const std::string & item_name, int item_price) :
    _name(item_name),
    _description("No description!"),
    _price(item_price)
{
}

void Item::SetDescription(const std::string& item_description)
{
    _description = item_description;
}

void Item::Display() const
{
    std::cout << "    " << "    " << " - " << _name << " [Price: " << _price << "]\n";
    std::cout << "    " << "    " << "    " << _description << "\n";
}
