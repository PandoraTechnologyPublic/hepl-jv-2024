﻿#include "Inventory.h"

#include <iostream>

void Inventory::AddItem(const Item & item)
{
    if (_numberOfItems < MAX_ITEMS)
    {
        _items[_numberOfItems] = item;

        ++_numberOfItems;
    }
    else
    {
        std::cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << "\n";
        std::cout << "!! ERROR: Inventory is full! !!" << "\n";
        std::cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << "\n";
    }
}

void Inventory::Display() const
{
    std::cout << "=================" << "\n";
    std::cout << "||  INVENTORY  ||" << "\n";
    std::cout << "=================" << "\n";

    for (int item_index = 0; item_index < _numberOfItems; ++item_index)
    {
        std::cout << "Item " << item_index << ": ";
        _items[item_index].Display();
        std::cout << "\n";
    }

    std::cout << "=================" << "\n";
}