﻿#pragma once
#include "Items/Armor.h"
#include "Items/Item.h"
#include "Items/Weapon.h"

const int MAX_WEAPON_COUNT = 2;
const int MAX_ARMOR_COUNT = 5;
const int MAX_ITEM_COUNT = 3;

class Inventory
{
private:
    Weapon _weapons[MAX_WEAPON_COUNT];
    int _weaponCount = 0;
    Armor _armors[MAX_ARMOR_COUNT];
    int _armorCount = 0;
    Item _items[MAX_ITEM_COUNT];
    int _itemCount = 0;
public:
    int GetDefenseValue() const;
    int GetAttackValue() const;

    void Display() const;

    void Add(const Weapon & weapon);
    void Add(const Armor & armor);
    void Add(const Item & item);
};
