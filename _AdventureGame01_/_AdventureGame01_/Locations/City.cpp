﻿#include "City.h"

#include <iostream>

City::City(const std::string& name) :
    Location(name)
{
}

void City::Enter()
{
    Location::Enter();

    while (true)
    {
        std::string player_choice;

        std::cout << "======================================" << '\n';
        std::cout << "1. Enter the shop" << '\n';
        std::cout << "2. Enter the inn" << '\n';
        std::cout << "3. Quit" << '\n';
        std::cout << "===================" << '\n';
        std::cout << "Enter your choice: " << '\n';
        std::cout << "===================" << '\n';
        std::cin >> player_choice;

        if (player_choice == "1")
        {
            std::cout << "===============================" << '\n';
            std::cout << "The door of the shop is closed!" << '\n';
            std::cout << "===============================" << '\n';
        }
        else if (player_choice == "2")
        {
            std::cout << "===============================" << '\n';
            std::cout << "The door of the inn is closed!" << '\n';
            std::cout << "===============================" << '\n';
        }
        else if (player_choice == "3")
        {
            std::cout << "=================" << '\n';
            std::cout << "You left the city" << '\n';
            std::cout << "=================" << '\n';

            break;
        }
        else
        {
            std::cout << "Invalid choice. Please try again." << '\n';
        }
    }

}
