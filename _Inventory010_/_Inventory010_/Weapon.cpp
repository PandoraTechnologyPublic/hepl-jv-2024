﻿#include "Weapon.h"

#include <iostream>

Weapon::Weapon() :
    _name("Uninitialised"),
    _description("Uninitialised"),
    _price(-1),
    _damageValue(-1)
{

}

Weapon::Weapon(const std::string & armor_name, int damage_value, int armor_price) :
    _name(armor_name),
    _description("No description!"),
    _price(armor_price),
    _damageValue(damage_value)
{
}

void Weapon::SetDescription(const std::string & weapon_description)
{
    _description = weapon_description;
}

void Weapon::Display() const
{
    std::cout << "    " << "    " << " - " << _name << " [Damage:" << _damageValue << "] [Price: " << _price << "]\n";
    std::cout << "    " << "    " << "    " << _description << "\n";
}
