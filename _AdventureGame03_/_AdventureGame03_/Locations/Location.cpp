﻿#include "Location.h"

#include <iostream>

Location::Location(const std::string & name) :
    _id(std::rand()),
    _name(name)
{
}

std::string Location::GetName() const
{
    return _name;
}

void Location::Enter(Character & character)
{
    std::cout << "=======================================================================" << '\n';
    std::cout << "|| Entering location NAME: " << _name <<" - LOCATION_ID: " << _id << '\n';
    std::cout << "=======================================================================" << '\n';
}
