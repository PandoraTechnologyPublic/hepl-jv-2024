﻿#include "Character.h"

#include <iostream>

#include "Enemy.h"

Character::Character(const std::string & name, int health) :
    _name(name),
    _health(health),
    _level(1)
{
}

int Character::GetLevel() const
{
    return _level;
}

int Character::GetHealth() const
{
    return _health;
}

const std::string & Character::GetName() const
{
    return _name;
}

void Character::Attack(Character & defender) const
{
    int attackValue = GetAttackValue();
    int defenseValue = defender.GetDefenseValue();
    int hitValue = std::max(0, attackValue - defenseValue);

    defender.TakeDamage(hitValue);
}

void Character::TakeDamage(int damage_value)
{
    _health = std::max(0, _health - damage_value);
}
