
#include "Inventory.h"

int main(int argc, char* argv[])
{
    Inventory warrior_inventory;
    Weapon sword("Sword", 10, 100);
    Armor plate_armor("Plate armor", 5, 150);
    Item ring("Ring of Emptiness", 5);

    warrior_inventory.Add(sword);
    warrior_inventory.Add(plate_armor);
    warrior_inventory.Add(ring);

    warrior_inventory.Display();

    return 0;
}
