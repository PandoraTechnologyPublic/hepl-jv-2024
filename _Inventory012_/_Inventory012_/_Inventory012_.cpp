
#include <iostream>

#include "Inventory.h"

int main(int argc, char* argv[])
{
    Inventory warrior_inventory;
    Weapon sword("Sword", 10, 100);
    Armor plate_armor("Plate armor", 5, 150);
    Item ring("Ring of Emptiness", 5);

    std::cout << "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n";

    sword.Display();

    std::cout << "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n";

    Item sword_as_item = sword;

    sword_as_item.Display();

    std::cout << "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n";

    Item & sword_as_referenced_item = sword;

    sword_as_referenced_item.Display();

    std::cout << "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n";

    return 0;
}
