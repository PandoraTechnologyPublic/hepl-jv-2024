
#include "Inventory.h"
#include "Item.h"

int main(int argc, char* argv[])
{
    Inventory warrior_inventory;

    Item helmet("Helmet", 3);
    Item armor("Armor", 5);
    Item boots("Boots", 2);

    warrior_inventory.AddItem(helmet);
    warrior_inventory.AddItem(armor);
    warrior_inventory.AddItem(boots);

    warrior_inventory.Display();

    Inventory mage_inventory;

    Item gloves("Gloves", 1);
    Item ring("Ring", 0);

    mage_inventory.AddItem(gloves);
    mage_inventory.AddItem(ring);
    mage_inventory.AddItem(armor);

    mage_inventory.Display();

    return 0;
}
