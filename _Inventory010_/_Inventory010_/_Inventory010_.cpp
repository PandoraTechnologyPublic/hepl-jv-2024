
#include <iostream>

#include "Armor.h"
#include "Inventory.h"
#include "Weapon.h"

int main(int argc, char* argv[])
{
    Inventory warrior_inventory;
    Weapon sword("Sword", 10, 100);

    warrior_inventory.AddWeapon(sword);

    Armor plate_armor("Plate armor", 5, 150);

    warrior_inventory.AddArmor(plate_armor);

    Item ring("Ring of Emptiness", 5);

    warrior_inventory.AddItem(ring);

    warrior_inventory.Display();

    warrior_inventory.Add(sword);
    warrior_inventory.Add(plate_armor);
    warrior_inventory.Add(ring);

    warrior_inventory.Display();

    return 0;
}
