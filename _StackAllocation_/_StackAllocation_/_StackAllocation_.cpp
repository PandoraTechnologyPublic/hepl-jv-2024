#include "Inventory.h"
#include "Item.h"

int main(int argc, char* argv[])
{
    Inventory inventory;

    Item & item = inventory.GetStackAllocatedItem();

    item.DisplayId();

    return 0;
}
