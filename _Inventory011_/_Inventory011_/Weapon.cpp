﻿#include "Weapon.h"

#include <iostream>


Weapon::Weapon() :
    _damageValue(-1)
{
}

Weapon::Weapon(const std::string & name, int damage_value, int price) :
    Item(name, price),
    _damageValue(damage_value)
{
}

int Weapon::GetDamageValue() const
{
    return _damageValue;
}

void Weapon::Display() const
{
    Item::Display();

    std::cout << "    " << "    " << "    " << "[Damage:" << _damageValue << "]\n";
}

