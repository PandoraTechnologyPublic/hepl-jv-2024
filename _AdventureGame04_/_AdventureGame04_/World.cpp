﻿#include "World.h"

#include <iostream>

#include "GamePrompt.h"

World::World() :
    _city("Liegus"),
    _playerPrompt()
{
    GameOption enter_the_city_game_option("Enter the city");
    GameOption exit_the_world_game_option("Exit the world");

    _playerPrompt.AddGameOption(enter_the_city_game_option);
    _playerPrompt.AddGameOption(exit_the_world_game_option);
}

void World::Explore()
{
    while (true)
    {
        _playerPrompt.DisplayPlayerChoices();

        std::string player_choice;

        int random_value = std::rand();
        std::cout << random_value << '\n';
        std::cout << "1. Enter the city" << '\n';
        std::cout << "2. Exit the world" << '\n';
        std::cout << "Enter your choice: ";
        std::cin >> player_choice;

        if (player_choice == "1")
        {
            //_city.Enter();
        }
        else if (player_choice == "2")
        {
            std::cout << "Goodbye!" << '\n';

            break;
        }
        else
        {
            std::cout << "Invalid choice. Please try again." << '\n';
        }
    }
}
