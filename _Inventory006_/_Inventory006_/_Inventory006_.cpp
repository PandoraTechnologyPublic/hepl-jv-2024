
#include "Inventory.h"
#include "Item.h"

int main(int argc, char* argv[])
{
    Inventory warrior_inventory;
    Item helmet;
    helmet._itemName = "Helmet";
    helmet._armorValue = 3;
    Item armor;
    armor._itemName = "Armor";
    armor._armorValue = 5;
    Item boots;
    boots._itemName = "Boots";
    boots._armorValue = 2;

    warrior_inventory.AddItem(helmet);
    warrior_inventory.AddItem(armor);
    warrior_inventory.AddItem(boots);

    warrior_inventory.Display();

    Inventory mage_inventory;
    Item gloves;
    gloves._itemName = "Gloves";
    gloves._armorValue = 1;
    Item ring;
    ring._itemName = "Ring";
    ring._armorValue = 0;

    mage_inventory.AddItem(gloves);
    mage_inventory.AddItem(ring);
    mage_inventory.AddItem(armor);

    mage_inventory.Display();

    return 0;
}
