#include <iostream>
#include <string>

const int MAX_ITEMS = 3;

class Item
{
public:
    std::string _itemName;
    int _itemQuantity;
};

class Inventory
{
public:
    Item _items[MAX_ITEMS];
    int _numberOfItems = 0;
};

void AddItem(Inventory & inventory, const std::string & item_name, int item_count)
{
    inventory._items[inventory._numberOfItems]._itemName = item_name;
    inventory._items[inventory._numberOfItems]._itemQuantity = item_count;

    ++inventory._numberOfItems;
}

void DisplayInventory(const Inventory & inventory)
{
    std::cout << "================" << "\n";
    for (int item_index = 0; item_index < inventory._numberOfItems; ++item_index)
    {
        std::cout << "Item " << item_index << ": " << inventory._items[item_index]._itemName << " [quantity " << inventory._items[item_index]._itemQuantity << "]\n";
    }
}

int main(int argc, char* argv[])
{
    Inventory warrior_inventory;

    AddItem(warrior_inventory, "Helmet", 1);
    AddItem(warrior_inventory, "Armor", 1);
    AddItem(warrior_inventory, "Boots", 1);

    DisplayInventory(warrior_inventory);

    Inventory mage_inventory;

    AddItem(mage_inventory, "Gloves", 1);
    AddItem(mage_inventory, "Ring", 1);
    AddItem(mage_inventory, "Armor", 1);

    DisplayInventory(warrior_inventory);
    DisplayInventory(mage_inventory);

    return 0;
}
