﻿#pragma once
#include <iostream>

#include "Item.h"

const int MAX_ITEMS = 3;

class Inventory
{
public:
    Item _items[MAX_ITEMS];
    int _numberOfItems = 0;
};

void AddItem(Inventory & inventory, const Item & item)
{
    if (inventory._numberOfItems < MAX_ITEMS)
    {
        inventory._items[inventory._numberOfItems] = item;

        ++inventory._numberOfItems;
    }
    else
    {
        std::cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << "\n";
        std::cout << "!! ERROR: Inventory is full! !!" << "\n";
        std::cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << "\n";
    }
}


void DisplayInventory(const Inventory & inventory)
{
    std::cout << "================" << "\n";
    for (int item_index = 0; item_index < inventory._numberOfItems; ++item_index)
    {
        std::cout << "Item " << item_index << ": " << inventory._items[item_index]._itemName << "(AC:" << inventory._items[item_index]._armorValue << ")\n";
    }
}