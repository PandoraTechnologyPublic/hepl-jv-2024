#include <iostream>
#include <string>

#include "World.h"

int main(int argc, char* argv[])
{
    while (true)
    {
        std::string player_choice;

        std::cout << "======================================" << '\n';
        std::cout << "1. Explore the world" << '\n';
        std::cout << "2. Quit the game" << '\n';
        std::cout << "===================" << '\n';
        std::cout << "Enter your choice: " << '\n';
        std::cout << "===================" << '\n';
        std::cin >> player_choice;

        if (player_choice == "1")
        {
            World world;

            world.Explore();
        }
        else if (player_choice == "2")
        {
            std::cout << "=================" << '\n';
            std::cout << "    GAME OVER" << '\n';
            std::cout << "=================" << '\n';

            break;
        }
        else
        {
            std::cout << "Invalid choice. Please try again." << '\n';
        }
    }

    return 0;
}
