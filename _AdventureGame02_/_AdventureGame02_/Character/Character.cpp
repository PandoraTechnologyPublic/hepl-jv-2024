﻿#include "Character.h"

Character::Character(const std::string & name, int money) :
    _name(name),
    _money(money)
{
}

int Character::GetMoney() const
{
    return _money;
}

void Character::Pay(int price)
{
    _money -= price;
}

std::string Character::GetName() const
{
    return _name;
}
