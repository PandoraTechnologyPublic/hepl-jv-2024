#include "Inventory.h"
#include "Item.h"

int main(int argc, char* argv[])
{
    Inventory warrior_inventory;
    Item helmet;
    helmet._itemName = "Helmet";
    helmet._armorValue = 3;
    Item armor;
    armor._itemName = "Armor";
    armor._armorValue = 5;
    Item boots;
    boots._itemName = "Boots";
    boots._armorValue = 2;

    AddItem(warrior_inventory, helmet);
    AddItem(warrior_inventory, armor);
    AddItem(warrior_inventory, boots);

    DisplayInventory(warrior_inventory);

    Inventory mage_inventory;
    Item gloves;
    gloves._itemName = "Gloves";
    gloves._armorValue = 1;
    Item ring;
    ring._itemName = "Ring";
    ring._armorValue = 0;

    AddItem(mage_inventory, gloves);
    AddItem(mage_inventory, ring);
    AddItem(mage_inventory, armor);
    AddItem(mage_inventory, armor);

    DisplayInventory(warrior_inventory);
    DisplayInventory(mage_inventory);

    return 0;
}
