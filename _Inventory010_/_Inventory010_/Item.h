﻿#pragma once
#include <string>

class Item
{
private:
    std::string _name;
    std::string _description;
    int _price;

public:
    Item();
    Item(const std::string & item_name, int item_price);

    void SetDescription(const std::string & item_description);

    void Display() const;
};
