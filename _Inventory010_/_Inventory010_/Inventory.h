﻿#pragma once
#include "Armor.h"
#include "Item.h"
#include "Weapon.h"

const int MAX_ITEM_COUNT = 3;
const int MAX_WEAPON_COUNT = 2;
const int MAX_ARMOR_COUNT = 5;

class Inventory
{
private:
    Item _items[MAX_ITEM_COUNT];
    int _itemCount = 0;
    Weapon _weapons[MAX_WEAPON_COUNT];
    int _weaponCount = 0;
    Armor _armors[MAX_ARMOR_COUNT];
    int _armorCount = 0;

public:
    void AddWeapon(const Weapon & weapon);
    void AddArmor(const Armor & armor);
    void AddItem(const Item & item);
    void Add(const Weapon & weapon);
    void Add(const Armor & armor);
    void Add(const Item & item);

    void Display() const;
};