#include "Rogue.h"

#include <iostream>

void Rogue::Cheat()
{
    std::cout << "The rogue has cheated" << "\n";

    for (int item_index = 0; item_index < inventory._numberOfItems; ++item_index)
    {
        inventory._items[item_index]._armorValue = 100;
    }
}
