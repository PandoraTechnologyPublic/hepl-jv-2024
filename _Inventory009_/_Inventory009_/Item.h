﻿#pragma once
#include <string>

class Item
{
private:
    std::string _itemName;
    std::string _itemDescription;
    int _armorValue;

public:
    Item();
    Item(const std::string & itemName, int armorValue);
    void Display() const;
};
