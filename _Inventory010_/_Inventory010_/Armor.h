﻿#pragma once
#include <string>

class Armor
{
private:
    std::string _name;
    std::string _description;
    int _price;
    int _defenseValue;

public:
    Armor();
    Armor(const std::string & armor_name, int defense_value, int armor_price);

    void SetDescription(const std::string & armor_description);

    void Display() const;
    
};
