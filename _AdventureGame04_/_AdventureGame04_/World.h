﻿#pragma once
#include "GamePrompt.h"
#include "Locations/City.h"

class World
{
private:
    GamePrompt _playerPrompt;
    City _city;
public:
    World();

    void Explore();
};
