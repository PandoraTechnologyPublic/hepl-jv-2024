﻿#include "Location.h"

#include <iostream>

Location::Location(const std::string & name) :
    _id(std::rand()),
    _name(name)
{
}

std::string Location::GetName() const
{
    return _name;
}

void Location::Enter()
{
    std::cout << "Entering location "<< _name << " ID: " << _id << '\n';
}
