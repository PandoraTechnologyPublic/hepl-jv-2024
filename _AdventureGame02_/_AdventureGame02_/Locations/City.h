﻿#pragma once
#include "Location.h"
#include "Shop.h"

class City : public Location
{
private:
    Shop _shop;
public:
    City(const std::string & name);

    void Enter(Character & character) override;
};
