﻿#pragma once
#include <string>

class Location
{
private:
    int _id;
    std::string _name;

public:
    Location(const std::string & name);

    std::string GetName() const;

    virtual void Enter();
};
