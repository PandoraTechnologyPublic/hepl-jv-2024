﻿#include "Armor.h"

#include <iostream>

Armor::Armor() :
    Item(),
    _defenseValue(-1)
{
}

Armor::Armor(const std::string& name, int defense_value, int price) :
    Item(name, price),
    _defenseValue(defense_value)
{
}

void Armor::Display() const
{
    Item::Display();

    std::cout << "    " << "    " << "    " << "[Defense:" << _defenseValue << "]\n";
}

int Armor::GetDefenseValue() const
{
    return _defenseValue;
}
