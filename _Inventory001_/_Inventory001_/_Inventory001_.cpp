#include <iostream>
#include <string>

const int MAX_ITEMS = 3;

class Inventory
{
public:
    std::string _itemNames[MAX_ITEMS];

    int _itemQuantities[MAX_ITEMS] = {0};
    int _numberOfItems = 0;
};

void AddItem(Inventory & inventory, const std::string & item_name, int item_count)
{
    inventory._itemNames[inventory._numberOfItems] = item_name;
    inventory._itemQuantities[inventory._numberOfItems] = item_count;

    ++inventory._numberOfItems;
}

void DisplayInventory(const Inventory & inventory)
{
    std::cout << "================" << "\n";
    for (int item_index = 0; item_index < inventory._numberOfItems; ++item_index)
    {
        std::cout << "Item " << item_index << ": " << inventory._itemNames[item_index] << " [quantity " << inventory._itemQuantities[item_index] << "]\n";
    }
}

int main(int argc, char* argv[])
{
    Inventory warrior_inventory;

    AddItem(warrior_inventory, "objet 0", 1);
    AddItem(warrior_inventory, "objet 1", 1);
    AddItem(warrior_inventory, "objet 2", 1);

    DisplayInventory(warrior_inventory);

    Inventory mage_inventory;

    AddItem(mage_inventory, "objet 3", 1);
    AddItem(mage_inventory, "objet 1", 1);
    AddItem(mage_inventory, "objet 4", 1);

    DisplayInventory(warrior_inventory);
    DisplayInventory(mage_inventory);

    return 0;
}
