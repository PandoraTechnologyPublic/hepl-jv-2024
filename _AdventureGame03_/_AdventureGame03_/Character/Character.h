﻿#pragma once
#include <string>

class Enemy;

class Character
{
private:
    std::string _name;
    int _health;
    int _level;
public:
    Character(const std::string & name, int health);

    int GetLevel() const;
    int GetHealth() const;
    const std::string & GetName() const;

    virtual int GetAttackValue() const = 0;
    virtual int GetDefenseValue() const = 0;

    virtual void Attack(Character & character) const;

    void TakeDamage(int damage_value);
};
