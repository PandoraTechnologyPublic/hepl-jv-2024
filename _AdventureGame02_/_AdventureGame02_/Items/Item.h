﻿#pragma once
#include <string>

class Item
{
private:
    std::string _name;
    int _price;

public:
    Item();
    Item(const std::string & name, int price);

    std::string GetName() const;
    int GetPrice() const;

    void Display() const;
};
