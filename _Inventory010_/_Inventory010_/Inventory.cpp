﻿#include "Inventory.h"

#include <iostream>

void Inventory::AddWeapon(const Weapon & weapon)
{
    if (_weaponCount < MAX_WEAPON_COUNT)
    {
        _weapons[_weaponCount] = weapon;
        ++_weaponCount;
    }
    else
    {
        std::cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << "\n";
        std::cout << "!! ERROR: Weapon inventory is full! !!" << "\n";
        std::cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << "\n";
    }
}

void Inventory::AddArmor(const Armor & armor)
{
    if (_armorCount < MAX_ARMOR_COUNT)
    {
        _armors[_armorCount] = armor;
        ++_armorCount;
    }
    else
    {
        std::cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << "\n";
        std::cout << "!! ERROR: Armor Inventory is full! !!" << "\n";
        std::cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << "\n";
    }
}

void Inventory::AddItem(const Item & item)
{
    if (_itemCount < MAX_ITEM_COUNT)
    {
        _items[_itemCount] = item;
        ++_itemCount;
    }
    else
    {
        std::cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << "\n";
        std::cout << "!! ERROR: Item Inventory is full! !!" << "\n";
        std::cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << "\n";
    }
}

void Inventory::Add(const Weapon& weapon)
{
    AddWeapon(weapon);
}

void Inventory::Add(const Armor& armor)
{
    AddArmor(armor);
}

void Inventory::Add(const Item& item)
{
    AddItem(item);
}


void Inventory::Display() const
{
    std::cout << "    Inventory:\n";
    std::cout << "    " << "  " << "Weapons:\n";

    if (_weaponCount <= 0)
    {
        std::cout << "    " << "    " << "  " << "NO WEAPONS\n";
    }
    else
    {
        for (int weapon_index = 0; weapon_index < _weaponCount; ++weapon_index)
        {
            _weapons[weapon_index].Display();
        }
    }

    std::cout << "    " << "  " << "Armors:\n";

    if (_armorCount <= 0)
    {
        std::cout << "    " << "    " << "  " << "NO ARMORS\n";
    }
    else
    {
        for (int armor_index = 0; armor_index < _armorCount; ++armor_index)
        {
            _armors[armor_index].Display();
        }
    }

    std::cout << "    " << "  " << "Items:\n";

    if (_itemCount <= 0)
    {
        std::cout << "    " << "    " << "  " << "NO ITEMS\n";
    }
    else
    {
        for (int item_index = 0; item_index < _itemCount; ++item_index)
        {
            _items[item_index].Display();
        }
    }
    std::cout << "===================================================\n";
}
