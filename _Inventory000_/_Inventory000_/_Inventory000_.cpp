#include <iostream>
#include <string>

const int MAX_ITEMS = 3;

std::string itemNames[MAX_ITEMS];

int itemQuantities[MAX_ITEMS] = {0};
int numberOfItems = 0;

void AddItem(const std::string & item_name, int item_count)
{
    itemNames[numberOfItems] = item_name;
    itemQuantities[numberOfItems] = item_count;

    ++numberOfItems;
}

void DisplayInventory()
{
    for (int item_index = 0; item_index < numberOfItems; ++item_index)
    {
        std::cout << "Item " << item_index << ": " << itemNames[item_index] << " [quantity " << itemQuantities[item_index] << "]\n";
    }
}

int main(int argc, char* argv[])
{
    AddItem("Object 1", 1);
    AddItem("Object 2", 1);
    AddItem("Object 3", 1);

    DisplayInventory();

    return 0;
}
